#include "GameScene.hpp"
#include "Paddle.hpp"
#include "Disc.hpp"
#include <BYS/UI/Text.hpp>

/////////////////////////////////
GameScene::GameScene (RenderWindow *window) : Scene (window), paused (false)
{
	setName ("GameScene");
	window->setMouseCursorVisible (false);

	///// field
	auto *field = new Sprite (AssetManager::getTexture ("gameplay/field.png"));
	field->setName ("field");
	setBackgrounColor (Color::White);
	addChild (field);

	///// Player 1
	auto *player1 = new Paddle (Keyboard::D, Keyboard::A, Keyboard::W, Keyboard::S,
								Vector2f (windowSize.x * 0.4f, 0), &windowSize);
	player1->setName ("Left Paddle");
	player1->setTag (Player1);
	player1->setOriginCenter ();
	player1->setPosition (windowSize.x * 0.1f, windowSize.y * 0.5f);
	addChild (player1);

	///// Player 2
	auto *player2 = new Paddle (Keyboard::Right, Keyboard::Left, Keyboard::Up, Keyboard::Down,
								Vector2f (windowSize.x, windowSize.x * 0.6f), &windowSize);
	player2->setName ("Right Paddle");
	player2->setTag (Player2);
	player2->setOriginCenter ();
	player2->setPosition (windowSize.x * 0.9f, windowSize.y * 0.5f);
	addChild (player2);

	///// Disc
	auto *disc = new Disc (player1, player2, &windowSize);
	disc->setName ("Disc");
	disc->setTag (DiscID);
	disc->setOriginCenter ();
	disc->setPosition (windowSize.x * 0.5f, windowSize.y * 0.5f);
	addChild (disc);

	///// Score
	StyleSheet gameSceneStyle ("css/GameScene.css");
	const auto &scoreProperties = gameSceneStyle.getProperties ("Score");

	Text *player1Score = new Text (this);
	player1Score->setTag (Player1Score);
	player1Score->setProperties (scoreProperties);
	player1Score->setProperties (gameSceneStyle.getProperties ("Player1Score"));

	Text *player2Score = new Text (this);
	player2Score->setTag (Player2Score);
	player2Score->setProperties (scoreProperties);
	player2Score->setProperties (gameSceneStyle.getProperties ("Player2Score"));

	///// Goals
	disc->goal.connect (bind (&GameScene::goal, this, placeholders::_1));

	///// Pause
	auto *pauseText = new Text (this);
	pauseText->setTag (PauseTextID);
	pauseText->setProperties (gameSceneStyle.getProperties ("Pause"));

	connectToEvent (Event::KeyPressed, bind (&GameScene::pause, this, placeholders::_1));
}

/////////////////////////////////
void GameScene::goal (Paddle *player)
{
	player->addScore ();
	Text *scoreToIncrease;

	if (player->getTag () == Player1)
		scoreToIncrease = static_cast <Text *> (getChildByTag (Player1Score));
	else
		scoreToIncrease = static_cast <Text *> (getChildByTag (Player2Score));

	int playerScore = player->getScore ();
	scoreToIncrease->setString (to_string (playerScore));

	if (playerScore == 5)
	{
		window->setMouseCursorVisible (true);
		close ();
		directorTask->type = DirectorTask::PopScene;
	}
	else
		static_cast <Disc *> (getChildByTag (DiscID))->restart ();
}

/////////////////////////////////
void GameScene::update (float deltaTime)
{
	if (! paused)
		Scene::update (deltaTime);
}

/////////////////////////////////
void GameScene::pause (const Event &event)
{
	if (event.key.code == Keyboard::P)
		static_cast <Text *> (getChildByTag (PauseTextID))->setVisible (paused = ! paused);
}
