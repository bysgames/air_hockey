#include "Disc.hpp"

#include <BYS/Core/AssetManager.hpp>

/////////////////////////////////
Disc::Disc (Paddle *paddle1, Paddle *paddle2, Vector2f *windowSize) :
			velocityX (500), velocityY (500), angle (100), paddle1 (paddle1), paddle2 (paddle2), windowSize (windowSize)
{
	auto &texture = AssetManager::getTexture ("gameplay/disc.png");
	setTexture (texture);
	radius = texture.getSize ().x * 0.5f;
}

/////////////////////////////////
#include "Paddle.hpp"
#include <math.h>
void Disc::checkCollisionsWith (Paddle *paddle)
{
	const auto &position = getPosition ();
	const auto &paddlePosition = paddle->getPosition ();

	const auto &distance = position - paddlePosition;
	float penetration = sqrt (distance.x * distance.x + distance.y * distance.y);
	bool areColliding = penetration < radius + paddle->radius;

	if (areColliding)
	{
		float angle = acos (fabs (distance.x) / penetration);
		penetration = radius + paddle->radius - penetration;

		if (distance.x >= 0.f && distance.y >= 0.f)
			move (cos (angle) * penetration + 0.5f, sin (angle) * penetration + 0.5f);
		else if (distance.x <= 0.f && distance.y >= 0.f)
			move (- cos (angle) * penetration - 0.5f, sin (angle) * penetration + 0.5f);
		else if (distance.x <= 0.f && distance.y <= 0.f)
			move (- cos (angle) * penetration - 0.5f, - sin (angle) * penetration - 0.5f);
		else if (distance.x >= 0.f && distance.y <= 0.f)
			move (cos (angle) * penetration + 0.5f, - sin (angle) * penetration - 0.5f);

		this->angle = angle;
		velocityX *= - 1.f;
		velocityY *= - 1.f;

		if (velocityX > 0.f)
			velocityX += 20.f;
		else
			velocityX -= 20.f;

		if (velocityY > 0.f)
			velocityY += 20.f;
		else
			velocityY -= 20.f;
	}
}

/////////////////////////////////
void Disc::update (float deltaTime)
{
	if (getAABB ().left < 5.f)
	{
		const auto &aabb = getAABB ();

		if (aabb.top >= windowSize->y * 0.25f && aabb.bottom <= windowSize->y * 0.75f)
			goal.emit (paddle2);

		move (5, 0);
		velocityX *= - 1.f;
	}

	if (getAABB ().right > windowSize->x - 5.f)
	{
		const auto &aabb = getAABB ();

		if (aabb.top >= windowSize->y * 0.25f && aabb.bottom <= windowSize->y * 0.75f)
			goal.emit (paddle1);

		move (-5, 0);
		velocityX *= - 1.f;
	}

	if (getAABB ().top < 5.f)
	{
		move (0, 5);
		velocityY *= - 1.f;
	}

	if (getAABB ().bottom > windowSize->y - 5.f)
	{
		move (0, - 5);
		velocityY *= - 1.f;
	}

	checkCollisionsWith (paddle1);
	checkCollisionsWith (paddle2);

	move (cos (angle) * velocityX * deltaTime, sin (angle) * velocityY * deltaTime);
}

/////////////////////////////////
void Disc::restart ()
{
	velocityX = velocityY = 500;
	setPositionV (*windowSize * 0.5f);
}
