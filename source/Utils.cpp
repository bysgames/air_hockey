#include "Utils.hpp"

void setOriginCenter (Entity &entity)
{
	const auto &aabb = entity.getAABB ();
	entity.setOrigin (aabb.width * 0.5f, aabb.height * 0.5f);
}
