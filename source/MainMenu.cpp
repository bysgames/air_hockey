#include "MainMenu.hpp"
#include <BYS/UI/Button.hpp>

/////////////////////////////////
MainMenu::MainMenu (RenderWindow *window) : Scene (window)
{
	setName ("MainMenu");
	StyleSheet mainMenuCSS ("css/MainMenu.css");

	// Scene's title
	auto *title = new Text (this);
	title->setProperties (mainMenuCSS.getProperties ("title"));

	// Background
	auto *background = new Sprite (AssetManager::getTexture ("gameplay/field.png"));
	background->setZOrder (-1);
	addChild (background);
	setBackgrounColor (Color::White);

	/////////////////// Buttons
	const auto &buttonProperties = mainMenuCSS.getProperties ("button");

	// Play button
	auto *playButton = new Button (this);
	playButton->setProperties (buttonProperties);
	playButton->setProperties (mainMenuCSS.getProperties ("playButton"));

	playButton->onMouseEnter += bind (&Button::setScale, playButton, 1.05f, 1.05f);
	playButton->onMouseLeave += bind (&Button::setScale, playButton, 1, 1);
	playButton->onLeftClick  += bind (&MainMenu::changeScene, this, Pong::GameScene);

	// Multiplayer button
	auto *multiplayerButton = new Button (this);
	multiplayerButton->setProperties (buttonProperties);
	multiplayerButton->setProperties (mainMenuCSS.getProperties ("multiplayerButton"));

	multiplayerButton->onMouseEnter += bind (&Button::setScale, multiplayerButton, 1.05f, 1.05f);
	multiplayerButton->onMouseLeave += bind (&Button::setScale, multiplayerButton, 1, 1);

	// Quit button
	auto *quitButton = new Button (this);
	quitButton->setProperties (buttonProperties);
	quitButton->setProperties (mainMenuCSS.getProperties ("quitButton"));

	quitButton->onMouseEnter += bind (&Button::setScale, quitButton, 1.05f, 1.05f);
	quitButton->onMouseLeave += bind (&Button::setScale, quitButton, 1, 1);
	quitButton->onLeftClick  += bind (&Scene::close, this);
	quitButton->onLeftClick  += bind (&DirectorTask::setType, directorTask, DirectorTask::PopScene);
}

/////////////////////////////////
#include "GameScene.hpp"
void MainMenu::changeScene (Pong::Scene scene)
{
	switch (scene)
	{
		case Pong::GameScene:
			directorTask->type = DirectorTask::PushScene;
			directorTask->scene = new GameScene (window);
			close ();
	}
}
