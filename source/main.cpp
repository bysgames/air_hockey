#include "MainMenu.hpp"
#include <BYS/Game/Director.hpp>
#include "Assets.h"

int main ()
{
	auto *window = Director::getInstance ().getWindow ();
	window->create (VideoMode::getDesktopMode (), "Pong :)", Style::Fullscreen);
	window->setVerticalSyncEnabled (true);

	AssetManager assetManager;
	AssetManager::setAssetsPath (ASSETS);

	return Director::getInstance ().run (new MainMenu (window));
}
