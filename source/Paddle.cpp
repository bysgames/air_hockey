#include "Paddle.hpp"
#include <BYS/Core/AssetManager.hpp>

/////////////////////////////////
Paddle::Paddle (Keyboard::Key keyRight, Keyboard::Key keyLeft, Keyboard::Key keyUp, Keyboard::Key keyDown, const Vector2f &limits, 					Vector2f *windowSize) :
				keyRight (keyRight), keyLeft (keyLeft), keyUp (keyUp), keyDown (keyDown), velocity (400.f), limits (limits),
				score (0), windowSize (windowSize)
{
	const auto &texture = AssetManager::getTexture ("gameplay/paddle.png");
	setTexture (texture);
	radius = texture.getSize ().x * 0.5f;
}

/////////////////////////////////
void Paddle::update (float deltaTime)
{
	if (Keyboard::isKeyPressed (keyRight) && getAABB ().right < limits.x)
		move (velocity * deltaTime, 0.f);

	if (Keyboard::isKeyPressed (keyLeft) && getAABB ().left > limits.y)
		move (- velocity * deltaTime, 0.f);

	if (Keyboard::isKeyPressed (keyUp) && getAABB ().top > 1.f)
		move (0.f, - velocity * deltaTime);

	if (Keyboard::isKeyPressed (keyDown) && getAABB ().bottom < windowSize->y)
		move (0.f, velocity * deltaTime);
}

/////////////////////////////////
void Paddle::addScore ()
{
	++score;
}

/////////////////////////////////
int Paddle::getScore ()
{
	return score;
}
