#ifndef BYS_GAMES_PONG_GAME_SCENE_HPP
#define BYS_GAMES_PONG_GAME_SCENE_HPP

#include <BYS/Core/AssetManager.hpp>
#include <BYS/Game/Scene.hpp>
using namespace bys;
using namespace sf;
using namespace std;

#include "Utils.hpp"

class Paddle;

class GameScene : public Scene
{
public:
	GameScene (RenderWindow *window);

	enum elements {Player1, Player2, Player1Score, Player2Score, DiscID, PauseTextID};
	void goal (Paddle *annotator);
	void update (float deltaTime);
	void pause (const Event &event);

private:
	bool paused;
};

#endif // BYS_GAMES_PONG_GAME_SCENE_HPP
