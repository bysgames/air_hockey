#ifndef BYS_GAMES_PONG_UTILS_HPP
#define BYS_GAMES_PONG_UTILS_HPP

#include <BYS/Game/Entity.hpp>
using namespace bys;

namespace Pong {  enum Scene {MainMenu, GameScene};  }

void setOriginCenter (Entity &entity);

#endif // BYS_GAMES_PONG_UTILS_HPP
