#ifndef BYS_GAMES_PONG_DISC_HPP
#define BYS_GAMES_PONG_DISC_HPP

#include <SFML/System/Vector2.hpp>
using namespace sf;

#include <BYS/Animation/Sprite.hpp>
using namespace bys;

class Paddle;

class Disc : public Sprite
{
public:
	Disc (Paddle *paddle1, Paddle *paddle2, Vector2f *windowSize);

	void checkCollisionsWith (Paddle *paddle);
	void update (float deltaTime);

	Signal <Paddle *> goal;
	void restart ();

private:
	float velocityX, velocityY;
	float radius;
	float angle;
	Paddle *paddle1, *paddle2;
	Vector2f *windowSize;
};

#endif // BYS_GAMES_PONG_DISC_HPP
