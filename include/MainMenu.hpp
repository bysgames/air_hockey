#ifndef BYS_GAMES_PONG_MAIN_MENU_HPP
#define BYS_GAMES_PONG_MAIN_MENU_HPP

#include <BYS/Core/AssetManager.hpp>
#include <BYS/Game/Scene.hpp>
using namespace bys;
using namespace sf;
using namespace std;

#include "Utils.hpp"

class MainMenu : public Scene
{
public:
	MainMenu (RenderWindow *window);
	void changeScene (Pong::Scene scene);
};

#endif // BYS_GAMES_PONG_MAIN_MENU_HPP
