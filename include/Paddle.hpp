#ifndef BYS_GAMES_PONG_PADDLE_HPP
#define BYS_GAMES_PONG_PADDLE_HPP

#include <BYS/Animation/Sprite.hpp>
using namespace bys;

#include <SFML/Window/Keyboard.hpp>
using namespace sf;

class Paddle : public Sprite
{
public:
	Paddle (Keyboard::Key keyRight, Keyboard::Key keyLeft, Keyboard::Key keyUp, Keyboard::Key keyDown, const Vector2f &limits,
			Vector2f *windowSize);

	void update (float deltaTime);
	void addScore ();
	int getScore ();

	friend class Disc;

private:
	float velocity, radius;
	Keyboard::Key keyRight;
	Keyboard::Key keyLeft;
	Keyboard::Key keyUp;
	Keyboard::Key keyDown;
	Vector2f limits;
	Vector2f *windowSize;

	int score;
};

#endif // BYS_GAMES_PONG_PADDLE_HPP
